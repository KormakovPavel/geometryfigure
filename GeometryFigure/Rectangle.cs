﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeometryFigure
{
    class Rectangle
    {
        readonly int width;
        readonly int height;

        public Rectangle(int _width, int _height)
        {
            width = _width;
            height = _height;
        }

        public static Rectangle operator +(Rectangle rectangle, int i)
            => new Rectangle(rectangle.width + i, rectangle.height + i);

        public static Rectangle operator -(Rectangle rectangle, int i)
            => new Rectangle(rectangle.width - i, rectangle.height - i);

        public int GetSquare()
            => width * height;
        
        public int GetPerimeter()
            => 2 * (width + height);        

        public int this[string param]
        {
            get
            {
                return param switch
                {
                    "Площадь" => GetSquare(),
                    "Периметр" => GetPerimeter(),
                    _ => 0,
                };
            }
        }
    }
}
