﻿using System;

namespace GeometryFigure
{
    class Program
    {
        static void Main()
        {
            Console.Write("Введите ширину прямоугольника: ");
            Int32.TryParse(Console.ReadLine(), out int width);
            Console.Write("Введите длину прямоугольника: ");
            Int32.TryParse(Console.ReadLine(), out int height);
            Rectangle rectangle = new Rectangle(width, height);

            Console.Write("Если хотите увеличить прямоуголник введите +, если уменьшить -: ");
            string Action = Console.ReadLine();
            Rectangle MutableRectangle = null;
            if (Action.Include("\\+"))
            {
                Console.Write("На сколько хотите увеличить: ");
                int coefficient = Convert.ToInt32(Console.ReadLine());
                MutableRectangle = rectangle + coefficient;
            }
            else
            {
                if(Action.Include("\\-"))
                {
                    Console.Write("На сколько хотите уменьшить: ");
                    int coefficient = Convert.ToInt32(Console.ReadLine());
                    MutableRectangle = rectangle - coefficient;
                }                
            }               
           
            int SquareMutableRectangle = MutableRectangle?.GetSquare() ?? 0;
            int PerimeterMutableRectangle = MutableRectangle?.GetPerimeter() ?? 0;
            Console.WriteLine($"Площадь исходного прямоугольника: {rectangle["Площадь"]}");
            Console.WriteLine($"Периметр исходного прямоугольника: {rectangle["Периметр"]}");
            Console.WriteLine($"Площадь измененного прямоугольника: {SquareMutableRectangle}");
            Console.WriteLine($"Периметр измененного прямоугольника: {PerimeterMutableRectangle}");
            Console.ReadLine();
        }
    }
}
