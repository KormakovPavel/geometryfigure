﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace GeometryFigure
{
    public static class StringExtension
    {
        public static bool Include(this string str,string sudstr)
        {
            if (Regex.IsMatch(str, sudstr))
                return true;
            else
                return false;
        }       
    }
}
